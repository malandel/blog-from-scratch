<?php
session_start();
// Paramétrer les erreurs
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//Inclure le fichier fonctions
require ('lib/fonctions.php');
//Inclure le header
require ('parts/header.php');
//Afficher page
getPage();
//Inclure le footer
require ('parts/footer.php');
?>