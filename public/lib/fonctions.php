<?php
function getPage(){
	if(!isset($_GET['page'])){
		include 'pages/home.php';
	} else {
		include 'pages/'.$_GET['page'];
	}
}

function bandeau_connexion(){
	if (!isset($_POST['authentifier'])){
?>
		<!-- Formulaire d'authentification -->
		<div class="connexion_form">
			<p>Vous identifier : </p>
			<form action="." method="POST" id="connexion">
				<input type="text" name="login" placeholder="Votre login">
				<input type="password" name="password" placeholder="Votre mot de passe">
				<input type="submit" name="authentifier" value="Se connecter">
			</form>
			<p>Pas encore inscrit ? <a href="index.php?page=inscription.php">Vous enregistrer</a></p>
		</div>
<?php
	return $_SESSION['username'] = $_POST['login'];
	} 
	else {
		echo '<p>Bonjour <strong>' .$_SESSION['username'] . '</strong> !</p>';
	 }
		
}
?>
