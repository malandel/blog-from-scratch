<?php
//Accéder aux variables de connexion
require ('lib/configuration.php');
//Se connecter à la base de données
try{
    $bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch;charset=utf8', $config_login, $config_password);
}
catch (Exception $e)
{
    //Tester les erreurs (arrêter l'exécution de la page et afficher un message décrivant l'erreur)
    die('Erreur : ' . $e->getMessage());
}
//AFFICHER LE BANDEAU DE CONNEXION ou MESSAGE PERSONNALISE
bandeau_connexion(); 

// FILTRES
$filtres_auteurs = $bdd->query
(
    'SELECT *
    FROM authors

')
?>
<p>Filtrer par : </p>
<select name="auteur_select">
    <option name="auteur">Auteur</option>
    <?php
        foreach($filtres_auteurs as $auteur){
        $auteurComplete = $auteur['firstname'] . ' ' . $auteur['lastname'];
        echo '<option name="auteur">'. $auteurComplete . '</option>';
        }
    ?>
</select>

<?php
$filtres_categories = $bdd->query
(
    'SELECT *
    FROM categories

')
?>
<select name="categorie_select">
    <option name="categorie">Catégorie</option>
    <?php
        foreach($filtres_categories as $categorie){
        echo '<option name="categorie">'. $categorie['category'] . '</option>';
        }
    ?>
</select>

<?php
//Requête articles
$articlesAccueil= $bdd->query
(
    'SELECT title, content, image_url, published_at, reading_time, author_id, authors.firstname, authors.lastname, articles.id AS article_id_initial 
    FROM articles    
    JOIN  authors ON articles.author_id = authors.id
    ORDER BY published_at DESC'
);
?>
<?php
//Afficher les données 
foreach ($articlesAccueil as $articles){
?>
    <div class="article_sample">
        <div class="titre_article_sample">
            <strong><?=$articles['title']?></strong> <em>- reading time : <?=$articles['reading_time']?>' min </em>  
        </div>
        <div class="article_sample_inner">
            <div class="article_sample_inner_text">
                <!-- N'affiche que les 300 premiers caractères (substr) du contenu sans balises html (strip_tags) -->
                <p class="content"><?=substr(strip_tags($articles['content']),0, 300) . ' ...'?></p> 
                <p class="sous_titre_sample">
                    <!-- Convertit au format date PHP (date_create), puis affiche au format dd-mm-YYYY (date_format) -->
                    Published : <em><?=date_format(date_create($articles['published_at']), 'd-m-Y')?></em>  
                    | by <em><a href="#"><?=$articles['firstname'] . ' ' . $articles['lastname']?></a></em>
                    <?php
                    //Créer une variable pour stocker l'ID de chaque article
                    $article_id = $articles['article_id_initial'];
                    // Requête pour les catégories
                    $categoryData = $bdd->query(
                        'SELECT categories.category FROM articles_categories
                        JOIN categories ON articles_categories.category_id = categories.id
                        WHERE articles_categories.article_id = '.$article_id.'
                    '); 
                    // Boucler pour afficher les catégories 
                    while ($category = $categoryData->fetch()){
                    ?>
                    | <span class="categorie"><?=$category['category']?></span>
                    <?php
                    }
                    ?>
                </p>
                <a class="lire_suite" href=<?="index.php?page=article.php&article=".$article_id?>>Lire la suite</a>
            </div>
            <img src="<?=$articles['image_url']?>">
        </div>
    </div>

<?php
}
?>