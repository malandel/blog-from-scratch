<?php
//Accéder aux variables de connexion
require ('lib/configuration.php');
//Afficher bandeau connexion
bandeau_connexion();
//Se connecter à la base de données
try{
$bddArticle = new PDO('mysql:host=localhost;dbname=blogfromscratch;charset=utf8', $config_login, $config_password);
}
catch (Exception $e)
{
    //Tester les erreurs (arrêter l'exécution de la page et afficher un message décrivant l'erreur)
    die('Erreur : ' . $e->getMessage());
}
//Récupérer l'ID de l'article cliqué sur la page d'accueil
$article_unique_id = $_GET['article'];
//Requête article
$articleUnique = $bddArticle->query
(
    'SELECT title, content, image_url, published_at, reading_time, author_id, authors.firstname, authors.lastname, articles.id AS article_id_initial 
    FROM articles    
    JOIN  authors ON articles.author_id = authors.id
    WHERE articles.id = '.$article_unique_id.'
');
//Afficher les données 
foreach ($articleUnique as $article){
?>

<div id="article_unique">
    <div class="titre_article_unique">
        <strong><?=$article['title']?></strong> <em>- reading time : <?=$article['reading_time']?>' min</em>  
    </div>
    <p class="sous_titre_unique">
        <!-- Convertit au format date PHP (date_create), puis affiche au format dd-mm-YYYY (date_format) -->
        Published : <em><?=date_format(date_create($article['published_at']), 'd-m-Y')?></em>  
        | by <em><a href="#"><?=$article['firstname'] . ' ' . $article['lastname']?></a></em>
            <?php
            // Requête pour les catégories
            $categoryDataArticle = $bddArticle->query(
                'SELECT categories.category FROM articles_categories
                JOIN categories ON articles_categories.category_id = categories.id
                WHERE articles_categories.article_id = '.$article['article_id_initial'].'
            '); 
            // Boucler pour afficher les catégories 
            while ($categoryArticle = $categoryDataArticle->fetch()){
            ?>
        | <span class="categorie"><?=$categoryArticle['category']?></span>
            <?php
            }
            ?>
    </p>
    <img src="<?=$article['image_url']?>">
    <div class="contenu_article_unique">
        <p><?=$article['content'];?></p>
    </div>
</div>
<?php
}
?>